<?php


/**
 *  This displays the global settings for auto translate. It will create a field for each 3rd party translator included
 *  in the /translators directory of the sprawk module directory. When writing your own include file, you may include
 *  a fieldset for your translator by implementing the sprawk_TRANSLATOR_settings hook (replacing TRANSLATOR with the
 *  name of your .inc file).
 */
function sprawk_settings($form_state) {

  // drupal_set_header('Access-Control-Allow-Origin: *');

  $form = array();
  $data = db_fetch_object(db_query("SELECT COUNT(*) as total FROM {cache_sprawk}"));

  // Enable / disable module
  $form['sprawk_on'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Enables or disables all functions of the module.'),
    '#weight' => -20,
  );

  if (variable_get('sprawk_on', 0)) {
    $form['sprawk_on']['#attributes'] = array('checked' => 'checked');
  }

  // Debugging
  $form['debugging'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debugging'),
    '#collapsible' => FALSE,
    '#weight' => -10,
  );

  $form['debugging']['sprawk_profiling'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Enables or disables this module profiling.'),
  );

  if (variable_get('sprawk_profiling', 0)) {
    $form['debugging']['sprawk_profiling']['#attributes'] = array('checked' => 'checked');
  }

  $form['debugging']['sprawk_debugging'] = array(
    '#type' => 'checkbox', 
    '#title' => t('More debugging data in logs.'),
  );

  if (variable_get('sprawk_debugging', 0)) {
    $form['debugging']['sprawk_debugging']['#attributes'] = array('checked' => 'checked');
  }

  // Other settings
  $form['sprawk'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sprawk Team Configuration'),
    '#description' => t("These settings should correspond to an existing Sprawk team registered at the <a href='http://www.sprawk.com'>sprawk.com</a> website. If you don't have a Sprawk team account, <a href='http://www.sprawk.com/en/auth/signupTeam.action'>register now</a> for free."),
    '#collapsible' => FALSE
  );

  $form['sprawk']['sprawk_server'] = array(
    '#type' => 'select',
    '#title' => t('Sprawk server'),
    '#description' => t('The development server has newer features, but can be less stable.') . ' ' .
       t('Contact support@sprawk.com for more details'),
    '#weight' => -9,
    '#options' => array(
      "www.sprawk.com" => t('Production server'),
      "beta.sprawk.com" => t('Testing server'),
      "alpha.sprawk.com" => t('Development server')
      ),
    '#default_value' => variable_get('sprawk_server', 'www.sprawk.com')
  );

  $form['sprawk']['sprawk_groupcode'] = array(
    '#type' => 'textfield',
    '#size' => 16,
    '#title' => t('Team code'),
    '#description' => t('Your team\'s code.'),
    '#weight' => -7,
    '#default_value' => variable_get('sprawk_groupcode', '')
  );

  $form['sprawk']['sprawk_grouppassword'] = array(
    '#type' => 'textfield',
    '#size' => 48,
    '#title' => t('Team password'),
    '#description' => t('Your team\'s password in MD5 format.'),
    '#weight' => -5,
    '#default_value' => variable_get('sprawk_grouppassword', '')
  );

  // see: sprawk.js for action of button
  $form['sprawk']['test_pass_div'] = array(
    /*
    '#type' => 'button',
    '#button_type' => 'test-button',
    '#value' => t("Test connection"),
    '#prefix' => '<div class="sprawk-testpass">',
    '#suffix' => '&nbsp;<span id="sprawk-testpassresult"></span></div>',
      */
    '#type' => 'submit',
    '#value' => t('Test connection'),
    '#prefix' => '<div class="btn-group">',
    '#suffix' => '</div>',
    '#submit' => array('sprawk_testpass_submit'),
  );

  $form['sprawk']['sprawk_topic'] = array(
    '#type' => 'select',
    '#title' => t('Topic'),
    '#description' => t('The topic to store locale strings.'),
    '#options' => array_merge(array(
      "" => t('Select a topic')), _sprawk_fetch_topics()),
    '#default_value' => variable_get('sprawk_topic', '')
  );

  $form['sprawk']['sprawk_hf'] = array(
    '#type' => 'select',
    '#title' => t('Highlight failed'),
    '#description' => t('How to display missing translations to visitors.') . ' ' .
      l('Read more', 'http://www.sprawk.com/help/content/highlighting-failed-translations'),
    '#weight' => -7,
    '#options' => array(
      "off" => t('Off'),
      "basic" => t('Basic'),
    ),
    '#default_value' => variable_get('sprawk_hf', 'basic')
  );

  $form['sprawk']['sprawk_mb'] = array(
    '#type' => 'select',
    '#title' => t('Missing behaviour'),
    '#description' => t('How to react to missing translations.'),
    '#weight' => -7,
    '#options' => array(
      "default" => t('Default'),
      "mt" => t('Force machine translation'),
      "original" => t('Show original text'),
      "skip" => t('Skip text in translated page'),
    ),
    '#default_value' => variable_get('sprawk_mb', 'default')
  );

  $form['sprawk']['sprawk_timeout'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 2,
    '#title' => t('Request Timeout'),
    '#description' => t('How many seconds to wait for Sprawk API response before returning untranslated. Default is 30 seconds'),
    '#default_value' => variable_get('sprawk_timeout', 30)    
  );

  $form['sprawk']['sprawk_cache_lifetime'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 2,
    '#title' => t('Cache Lifetime'),
    '#description' => t('How many days items should be saved on cache. Default is 30 days'),
    '#default_value' => variable_get('sprawk_cache_lifetime', '')    
  );

  $form['sprawk']['cache_count'] = array(
    '#type' => 'markup',
    '#value' => format_plural($data->total, 'There is 1 item currently in cache', 'There are @count items currently in cache'),
  );

  $form['sprawk']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Reset cache'),
    '#prefix' => '<div class="btn-group">',
    '#suffix' => '</div>',
    '#submit' => array('sprawk_clear_cache_submit'),
  );

  $form['sprawk']['ext_link'] = array(
    '#type' => 'markup',
    '#value' => '<div class="sprawk-prompt">' . 
      t('For free assistance configuring sprawk with Drupal contact <a href="@mailto">support</a> or visit the <a target="_blank" href="@helpurl">help pages</a>', array('@mailto' => 'mailto:support@sprawk.com', '@helpurl' => 'http://www.sprawk.com/help/')) . '</div>',
  );

  return system_settings_form($form);
}
