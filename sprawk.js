var sprawk = {

  /**
   * If true, write out debugging info to console.log
   */
  debug: false,

  /**
   * The default site language
   */
  defaultLang: 'en',

  /**
   * The current language of this page (this is set through sprawk_init)
   */
  userLocale: undefined

};

sprawk.poptopics = function (selectId, currVal) {
  var code = $("#edit-sprawk-groupcode").val();
  var pass = $("#edit-sprawk-grouppassword").val();
  $.getJSON("/sprawk/api/groupTopicsJSON.jsp", {groupCode: code, password: pass}, function (j) {
    var options = '';
    options += '<option value=\'\'>' + 'Select a topic' + '</option>';
    for (var i = 0; i < j.length; i++) {
      var topic = j[i];
      var topicCode = topic.optionValue;
      options += '<option value="' + topicCode + '" ' + (currVal == topicCode ? 'selected="selected"' : "") + '>' + topic.optionDisplay + '</option>';
    }
    $("select#" + selectId).html(options);
  });
};

sprawk.testpass = function () {
  var server = $("#edit-sprawk-server").val();
  var code = $("#edit-sprawk-groupcode").val();
  var pass = $("#edit-sprawk-grouppassword").val();
  var url = 'http://' + server + '/api/testGroupPass.jsp';
  $('#sprawk-testpassresult').load(url, {
    groupCode: code,
    password: encodeURIComponent(pass)
  });
};

/**
 * Helper to perform safe console logging
 */
sprawk.log = function (value, key) { // debug
  if (this.debug && console && console.log) {
    key = key || "sprawk debugging";
    console.log(key);
    console.log(value);
  }
};

/**
 *
 * @param sourceText
 */
sprawk.t = function (sourceText) {
  var retVal = sourceText;

  sprawk.log(arguments, 'arguments');

  if (this.userLocale == this.defaultLang) {
    // no need to translate, but we need to build the string if it's parameterized
    /* This will only work well as long as there are no gaps in the sequence of parameters and the sequence
     begins at 0. I.e., 'my text {0} {2}' won't work as there is no {1} */
    for (var i = 1; i < arguments.length; i++) {
      retVal = retVal.replace(new RegExp('\\{' + (i - 1) + '\\}', 'g'), arguments[i]);
    }
    retVal = this.applyWikiFormatting(retVal);
  }
  else if (sourceText !== undefined && $.trim(sourceText).length !== 0) {
    // iterate through params
    var url = '/' + this.userLocale + '/sprawk/translate';
    var params = 't=' + encodeURIComponent(sourceText);
    for (var i = 1; i < arguments.length; i++) {
      sprawk.log(arguments[i], 'argument');
      params += '&param[]=' + encodeURIComponent(arguments[i]);
    }
    this.log(params, "params");
    jQuery.ajax({
      async: false,
      type: 'get',
      dataType: 'html',
      url: url,
      data: params,
      success: function (data, textStatus, XMLHttpRequest) {
        sprawk.log({data: data, textStatus: textStatus}, "fromAjax");
        retVal = data;
      }
    });
    sprawk.log(retVal, "afterAjax");
  }
  return retVal;
};

/* This function returns the original source, assembled from parameters if needed,
 * and invokes the callback function with the result of the translation. */
sprawk.aT = function (sourceText, callback) {
  var processed = sourceText;
  /* Apply pattern and parameters */
  for (var i = 2; i < arguments.length; i++) {
    processed = processed.replace(new RegExp('\\{' + (i - 2) + '\\}', 'g'), arguments[i]);
  }

  processed = sprawk.applyWikiFormatting(processed);

  if (this.userLocale != 'en') {
    if (sourceText !== undefined && $.trim(sourceText).length !== 0) {
      // call back to the sprawk.module page callback:  _sprawk_translate
      var url = '/' + userLocale + '/sprawk/translate';
      // send text
      var params = 't=' + encodeURIComponent(sourceText);
      // iterate through params which will be fetched with $_REQUEST['param']
      for (var i = 2; i < arguments.length; i++) {
        params += '&param[]=' + encodeURIComponent(arguments[i]);
      }
      jQuery.ajax({
        async: true,
        type: 'get',
        url: url,
        dataType: 'html',
        data: params,
        success: function (data) {
          callback(data);
        }
      });
    }
  }
  else {
    //Maybe we should execute callback() here as well? Or let it depend on a new boolean arg. /Peter
  }
  return processed;
};

sprawk.applyWikiFormatting = function (text) {
  text = text.replace(/\*([^<>]*?)\*/g, '<b>$1</b>'); //Bold text
  text = text.replace(/#([^<>]*?)#/g, '<em>$1</em>'); //Emphasized text
  text = text.replace(/\/([^<>]*?)\//g, '<i>$1</i>'); //Italic text
  text = text.replace(/\[(sprawk)\]/i, '<span class="lowlight">$1</span>'); //Style the sprawk "logo"
  return text;
};

/* This function takes a string in the form of a variable name (including "complex"
 * like myObj.property). The variable "root" must be global but the window scope should
 * not be explicit. Second parameter is the string to be translated. Third argument is
 * an optional callback function to be invoked when the translation is available. Any additional
 * arguments are passed as translation parameters to aT() */
sprawk.assignAT = function (identifier, value, callbackOrFirstParam) {
  /* Force dot notation for easy splitting, if not in that format already */
  identifier = identifier.replace(/^\s*\[(.*?)\]\s*$/, '$1'); //Remove surrounding brackets
  identifier = identifier.replace(/\]\[/, '.'); //Replace "delimiting" brackets with dots
  var parts = identifier.split('.');

  /* If no value is passed, use the value already in the named variable */
  if (typeof value == 'undefined' || value === null || value === '') {
    value = this.getVar(parts);
  }

  /* Check to see if third argument exists and if so if it's a function or a translation param */
  var firstParam = 2;
  var extraCallback = function () {
  };
  if (typeof callbackOrFirstParam == 'function') {
    extraCallback = callbackOrFirstParam;
    firstParam = 3;
  }

  /* Orignally we used the static Array.concat() but that was not widely supported yet */
  var args = [];
  args.push(value);
  args.push(function (text) {
    sprawk.assignVar(parts, text);
    extraCallback(text);
  });
  args.concat(this.asArray(arguments).slice(firstParam));
  /* Returns the value that is also assigned to the
   * variable named in identifier (parts). The value is the un-translated but parameter-parsed
   * string that aT() returns. aT() is provided a callback function that assigns the translated
   * value to the variable named in identifier once returned from server.
   */
  return this.assignVar(parts, this.aT.apply(sprawk, args));
};

sprawk.getVar = function (parts) {
  var cur = window;
  for (var i = 0; i < parts.length; i++) {
    cur = cur[parts[i]];
  }
  return cur;
};

sprawk.assignVar = function (parts, value) {
  var cur = window;
  for (var i = 0; i < parts.length - 1; i++) {
    cur = cur[parts[i]];
  }
  cur[parts[i]] = value;
  return value; //Not needed but useful
};

sprawk.asArray = function (args) {
  var arr = [];
  for (var i = 0; i < args.length; i++) {
    arr[i] = args[i];
  }
  return arr;
};

sprawk.countWords = function (text) {
  if (!text) return 0;
  if (text.charAt(0) === ' ') {
    text = text.substring(1, text.length);
  }
  if (text.charAt(text.length - 1) === ' ') {
    text = text.substring(0, text.length - 1);
  }

  text = text.replace(/[!,;:\.\-\+=]+|[\r\n]+/g, ' ');
  text = text.replace(/\s\s+/g, ' ');

  var wc;
  if ($.trim(text).length === 0) {
    wc = 0;
  }
  else {
    var words = text.split(" ");
    wc = words.length;
  }
  return wc;
};

/**
 *
 * @param min
 * @param max
 * @param id the CSS ID (not name)
 * @param silent
 * @return true if the requirements are met, i.e. > min words
 */
sprawk.getWords = function (min, max, id, silent) {

  if (silent === undefined) {
    silent = false;
  }

  if (id === null || id === '') {
    return false;
  }

  if (!window.wordCountCache) {
    wordCountCache = {};
  }
  if (!wordCountCache[id]) {
    wordCountCache[id] = {};
  }

  var text = $('#' + id).val();
  var wc = sprawk.countWords(text);

  var statusDiv = $('#' + id + '_status');

  var wordsWord = (wc == 1 ? 'word' : 'words');
  var wordsWordMin = (min == 1 ? 'word' : 'words');
  var wordsWordMax = (max == 1 ? 'word' : 'words');

  if (!wordCountCache[id].lastWc || wc != wordCountCache[id].lastWc) {
    wordCountCache[id].lastWc = wc;

    if (wc < min) {
      if (!silent) {
        /*statusDiv.html(aT('Currently *{0}* ' + wordsWord + '. Please enter at least {1} ' + wordsWordMin + '.', function(translated){
         statusDiv.html(translated);
         }, wc, min));*/
        statusDiv.aTHtml('Currently *{0}* ' + wordsWord + '. Please enter at least {1} ' + wordsWordMin + '.', wc, min);
        statusDiv.removeClass().addClass("problem");
      }
      wordCountCache[id].lastRetVal = false;
      return false;
    } else if (wc > max && max != -1) {
      if (!silent) {
        // disableAddButton();
        statusDiv.html(sprawk.aT('Currently *{0}* ' + wordsWord + '. Please keep content under {1} ' + wordsWordMax + '.', function (translated) {
          statusDiv.html(translated);
        }, wc, max));
        statusDiv.removeClass().addClass("error");
      }
      wordCountCache[id].lastRetVal = false;
      return false;
    } else {
      if (!silent) {
        var trans = sprawk.aT('Currently *{0}* ' + wordsWord + '. Word count within target range.', function (translated) {
          statusDiv.html(translated);
        }, wc);
        statusDiv.html(trans);
        statusDiv.removeClass().addClass("completed");
      }
      wordCountCache[id].lastRetVal = true;
      return true;
    }
  }
  //Word count hasn't changed so return last returned value
  else {
    return wordCountCache[id].lastRetVal;
  }
};

/**
 * Locale-sensitive sort
 */
sprawk.sort = function (arr) {

  arr.sort(function(a, b) {
     return a.localeCompare(b, sprawk.userLocale);
  });

};

jQuery.fn.aTHtml = function (text) {
  var me = this;
  if (!text) {
    text = me.html();
  }
  /* Orignally we used the static Array.concat() but that was not widely supported yet */
  var args = [];
  args.push(text);
  args.push(function (translated) {
    me.html(translated);
  });
  args = args.concat(sprawk.asArray(arguments).slice(1));
  me.html(sprawk.aT.apply(sprawk, args));
  return this;
};

Drupal.behaviors.sprawk_test_pass = function (context) {
  $('.form-test-button').bind('click', function () {
    sprawk.testpass();
    return false;
  });
};
